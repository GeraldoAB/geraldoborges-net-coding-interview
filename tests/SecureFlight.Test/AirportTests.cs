using System.Threading.Tasks;
using Moq;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;
using SecureFlight.Infrastructure.Repositories;
using Xunit;
using FluentValidation;
using FluentAssertions;
using Microsoft.VisualStudio.TestPlatform.TestHost;
using Microsoft.AspNetCore.Mvc.Testing;
using SecureFlight.Api.Controllers;
using AutoMapper;
using SecureFlight.Api.MappingProfiles;
using SecureFlight.Api.Models;

namespace SecureFlight.Test
{
    public class AirportTests
    {
        private Airport _airport;
        public AirportTests()
        {
            _airport = new Airport();
            _airport.Code = "AAQ";
            _airport.Name = "Anapa Vityazevo";
            _airport.City = "Anapa";
            _airport.Country = "Russia";
        }

        [Fact]
        public async Task Update_Succeeds()
        {
            //Arrange
            var testingContext = new SecureFlightDatabaseTestContext();
            testingContext.CreateDatabase();
            var repository = new BaseRepository<Airport>(testingContext);
            var mockRepository = new Mock<IRepository<Airport>>();

            mockRepository.Setup(s => s.GetByIdAsync(It.IsAny<string>())).ReturnsAsync(It.IsAny<Airport>());
            mockRepository.Setup(s => s.Update(It.IsAny<Airport>())).Returns(It.IsAny<Airport>());

            var mapper = CreateIMapper();
            AirportsController controller = new AirportsController(mockRepository.Object, null, mapper);

            //TODO: Add test code here
            //Act

            await controller.Put("AAQ", mapper.Map<AirportDataTransferObject>(_airport));

            var result = repository.Update(_airport);

            //Assert
            mockRepository.Verify(m=> m.SaveChangesAsync(), Times.Once);
            
            result.Should().NotBeNull();
            testingContext.DisposeDatabase();
        }

        protected IMapper CreateIMapper()
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new DataTransferObjectsMappingProfile());
            });
            return mappingConfig.CreateMapper();
        }
    }
}
