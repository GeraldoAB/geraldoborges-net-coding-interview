﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using SecureFlight.Api.Models;
using SecureFlight.Api.Utils;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;

namespace SecureFlight.Api.Controllers;

[ApiController]
[Route("[controller]")]
public class FlightsController(IService<Flight> flightService,
    IRepository<Flight> flightRepository,
    IRepository<FlightStatus> flightStatusRepository,
    IRepository<Passenger> passengerRepository,
    IRepository<PassengerFlight> passengerFlightRepository,
    IMapper mapper)
    : SecureFlightBaseController(mapper)
{
    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<FlightDataTransferObject>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> Get()
    {
        var flights = await flightService.GetAllAsync();
        return MapResultToDataTransferObject<IReadOnlyList<Flight>, IReadOnlyList<FlightDataTransferObject>>(flights);
    }

    [HttpGet("origins/{origin}/destinations/{destination}")]
    [ProducesResponseType(typeof(IEnumerable<FlightDataTransferObject>), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(FlightDataTransferObject), StatusCodes.Status404NotFound)]
    public async Task<IActionResult> GetByOriginAndDestinationAsync(string origin, string destination)
    {
        if(origin is null || destination is null)
            return BadRequest("Origin and destination are required");

        var result = await flightService.FilterAsync(p=> p.OriginAirport == origin && p.DestinationAirport == destination);

        return MapResultToDataTransferObject<IReadOnlyList<Flight>, IReadOnlyList<FlightDataTransferObject>>(result);
    }

    [HttpPost("{flightId:long}/passengers/{passengerId}/transfer")]
    [ProducesResponseType(typeof(FlightDataTransferObject), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(FlightDataTransferObject), StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(FlightDataTransferObject), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> TransferPassengerAsync(long flightId, string passengerId, CancellationToken cancellationToken)
    {
        var flight = await flightRepository.GetByIdAsync(flightId);
        if (flight is null)
            return NotFound(string.Format(Core.Messages.CommonMessage.FLIGHT_NOT_FOUND, flightId));

        if (flight.FlightStatusId != Core.Enums.FlightStatus.Active &&
            flight.FlightStatusId != Core.Enums.FlightStatus.Delayed)
        {
            return BadRequest(string.Format(Core.Messages.CommonMessage.FLIGHT_NOT_AVAILABLE, flightId));
        }

        var passenger = await passengerRepository.GetByIdAsync(passengerId);
        if (passenger is null)
            return NotFound(string.Format(Core.Messages.CommonMessage.PASSENGER_NOT_FOUND, passengerId));

        var passengerFlight = await passengerFlightRepository.FilterAsync(p => p.PassengerId == passengerId && p.FlightId == flightId);
        if (passengerFlight.Any())
            return BadRequest(string.Format(Core.Messages.CommonMessage.PASSENGER_IS_ALREADY_ON_THIS_FLIGHT));

        var pf = new PassengerFlight();
        pf.PassengerId = passengerId;
        pf.FlightId = flightId;

        cancellationToken.ThrowIfCancellationRequested();

        await passengerFlightRepository.AddAsync(pf, cancellationToken);
        await passengerFlightRepository.SaveChangesAsync();


        return Created();
    }
}